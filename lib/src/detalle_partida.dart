import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DetallePartida extends StatefulWidget {
  const DetallePartida({ Key? key }) : super(key: key);

  @override
  _DetallePartidaState createState() => _DetallePartidaState();
}

class _DetallePartidaState extends State<DetallePartida> {
  List<charts.Series<Partida2, String>> _informacionPartida = [];

  _llenarLista(){
    var ronda1 = [
      Partida2(jugador: 'Pedro', cartasAzules: 5, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: 'Paco', cartasAzules: 2, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: 'Pepe', cartasAzules: 1, cartasVerdes: 0, cartasRosas: 0, cartasNegras: 0),
    ];

    var ronda2 = [
      Partida2(jugador: 'Pedro', cartasAzules: 5, cartasVerdes: 3, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: 'Paco', cartasAzules: 2, cartasVerdes: 1, cartasRosas: 0, cartasNegras: 0),
      Partida2(jugador: 'Pepe', cartasAzules: 1, cartasVerdes: 1, cartasRosas: 0, cartasNegras: 0),
    ];

    var ronda3 = [
      Partida2(jugador: 'Pedro', cartasAzules: 7, cartasVerdes: 3, cartasRosas: 5, cartasNegras: 5),
      Partida2(jugador: 'Paco', cartasAzules: 2, cartasVerdes: 1, cartasRosas: 1, cartasNegras: 1),
      Partida2(jugador: 'Pepe', cartasAzules: 2, cartasVerdes: 1, cartasRosas: 1, cartasNegras: 1),
    ];

    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasAzules,
        data: ronda1,
        id: '1',
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF337CFF))
      )
    );
    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasAzules,
        data: ronda2,
        id: '1',
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF1736C7))
      )
    );
    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasAzules,
        data: ronda3,
        id: '1',
        seriesCategory: '1',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF102587))
      )
    );

    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasVerdes,
        data: ronda2,
        id: '1',
        seriesCategory: '2',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF14D20A))
      )
    );
    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasVerdes,
        data: ronda3,
        id: '1',
        seriesCategory: '2',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF006907))
      )
    );

    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasRosas,
        data: ronda3,
        id: '1',
        seriesCategory: '3',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFFC23695))
      )
    );
    _informacionPartida.add(
      charts.Series(
        domainFn: (Partida2 partida, _) => partida.jugador,
        measureFn: (Partida2 partida, _) => partida.cartasNegras,
        data: ronda3,
        id: '1',
        seriesCategory: '4',
        fillPatternFn: (_,__) => charts.FillPatternType.solid,
        fillColorFn: (Partida2 partida, _) => charts.ColorUtil.fromDartColor(Color(0xFF111510))
      )
    );
  }

  @override
  void initState(){
    super.initState();
      _llenarLista();
    }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detalle de la partida'),
        backgroundColor: Colors.blue,
        ),
        body: Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Container(
                child: Center(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                          top: 8.0, 
                          right: 8.0,
                          ),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Text('Partida jugada el 22/11/2020',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 15.0
                            ),
                          ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Puntuaciones', 
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.w800,
                          ),),
                        ),
                        lugares(),
                        Container(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Cartas jugadas',
                                  style: TextStyle(
                                  fontSize: 24.0,
                                  fontWeight: FontWeight.bold),
                          ),
                        ),
                      Container(
                        child: Expanded(
                          child: charts.BarChart(
                            _informacionPartida,
                            animate: true,
                            barGroupingType: charts.BarGroupingType.groupedStacked,
                            animationDuration: Duration(seconds: 1),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Text('Ronda 1 '),
                          Icon(Icons.circle, color: Color(0xFF337CFF),),
                        ],
                      ),
                      Row(
                        children: [
                          Text('Ronda 2 '),
                          Icon(Icons.circle, color: Color(0xFF1736C7),),
                          Icon(Icons.circle, color: Color(0xFF14D20A),),
                        ],
                      ),
                      Row(
                        children: [
                          Text('Ronda 3 '),
                          Icon(Icons.circle, color: Color(0xFF102587),),
                          Icon(Icons.circle, color: Color(0xFF006907),),
                          Icon(Icons.circle, color: Color(0xFFC23695),),
                          Icon(Icons.circle, color: Color(0xFF111510),),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
        );
    }

  Widget lugares(){
    return Container(
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              //padding: EdgeInsets.only(top: 20.0, right: 2.0),
              child: Container(
                //height: 60.0,
                decoration: BoxDecoration(
                  color: Color(0xFF105D8F),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                  child: Column(
                    children: [
                      FaIcon(FontAwesomeIcons.crown, color: Colors.yellow,),
                        Text('Pedro', 
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white),
                          ),
                      Text('Puntuacion: ', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
                    ],
                  ),
                ),
              ),
            )
          ),
          SizedBox(
            height: 1,
            width: 1,
          ),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0, right: 8.0),
                    child: Center(
                      child: Container(
                        //height: 30.0,
                        decoration: BoxDecoration(
                          color: Color(0xFF919095),
                          borderRadius: BorderRadius.circular(5),
                  ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: Column(
                              children: [
                                Text('2\u00BA Paco', 
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        color: Colors.black,
                                        ),
                                      ),
                                Text('Puntuacion: ',style: TextStyle(fontWeight: FontWeight.bold),),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0, right: 8.0),
                    child: Center(
                      child: Container(
                        //height: 30.0,
                        decoration: BoxDecoration(
                          color: Color(0xFF919095),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                            child: Column(
                              children: [
                                Text('3\u00BA Pepe', 
                                      style: TextStyle(
                                        fontSize: 15.0,
                                        color: Colors.black,
                                        ),
                                      ),
                                Text('Puntuacion: ', style: TextStyle(fontWeight: FontWeight.bold),),
                              ],
                            ),
                          ),
                        ),
                      )
                    ),
                  ),
                ],
              ),
            )
          ),
        ],
      ),
    );
  }
}

class Partida2{
  String jugador;
  int cartasAzules;
  int cartasVerdes;
  int cartasRosas;
  int cartasNegras;

  Partida2({
    required this.jugador,
    required this.cartasAzules,
    required this.cartasVerdes,
    required this.cartasRosas,
    required this.cartasNegras
  });
}