import 'package:flutter/material.dart';
import 'package:vista_partida/src/detalle_partida.dart';
import 'package:vista_partida/src/principal.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ohanami',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),

      home: DetallePartida(),
    );
  }
}